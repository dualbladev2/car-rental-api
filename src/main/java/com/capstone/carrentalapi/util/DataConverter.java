package com.capstone.carrentalapi.util;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.capstone.carrentalapi.domain.model.Contract;
import com.capstone.carrentalapi.domain.model.TimeSlot;
import com.capstone.carrentalapi.dto.Car;
import com.capstone.carrentalapi.dto.contract.ContractResultById;
import com.capstone.carrentalapi.dto.timeslot.ReturnResult;
import com.capstone.carrentalapi.dto.timeslot.SearchCar;
import com.capstone.carrentalapi.dto.timeslot.TimeSlotRenting;

public class DataConverter {
	
	private static Car convertTemperatureDataToTemperatureDataDTO(com.capstone.carrentalapi.domain.model.Car carData) {
        if (Objects.isNull(carData)) {
            return null;
        }
        Car carDataDTO = new Car();
        carDataDTO.setIdCar(carData.getIdCar());
        carDataDTO.setStatus(carData.getStatus());
        carDataDTO.setLicensePlate(carData.getLicensePlate());
        carDataDTO.setDefaultPrice(carData.getDefaultPrice());
        carDataDTO.setDistanceLimit(carData.getDistanceLimit());
        carDataDTO.setOverDistancePrice(carData.getOverDistancePrice());
        carDataDTO.setSeat(carData.getSeat());
        carDataDTO.setManufacturedYear(carData.getManufacturedYear());
        carDataDTO.setRegistrationNumber(carData.getRegistrationNumber());
        carDataDTO.setPublicLatitude(carData.getPublicLatitude());
        carDataDTO.setPublicLongitude(carData.getPublicLongitude());
        carDataDTO.setRecentRating(carData.getRecentRating());
        carDataDTO.setTotalRating(carData.getTotalRating());
        carDataDTO.setDescription(carData.getDescription());
        carDataDTO.setRentingTerm(carData.getRentingTerm());
        carDataDTO.setFuelConsumption(carData.getFuelConsumption());
        carDataDTO.setCompletedTrip(carData.getCompletedTrip());
        carDataDTO.setCreatedDate(carData.getCreatedDate());
        carDataDTO.setLastUpdated(carData.getLastUpdated());
        return carDataDTO;
    }

    public static List<Car> convertTemperatureDataToListTemperatureDataDTOs(List<com.capstone.carrentalapi.domain.model.Car> temperatureDatas) {
        if (Objects.isNull(temperatureDatas)) {
            return null;
        }
        List<Car> currentTemperatureList = new ArrayList<>();
        temperatureDatas.forEach(temperatureData -> {
            currentTemperatureList.add(convertTemperatureDataToTemperatureDataDTO(temperatureData));
        });
        return currentTemperatureList;
    }
    
    /**
     * Convert Time Slot data
     */
    private static TimeSlotRenting convertTimeSlotDataToTimeSlotDataDTO(TimeSlot carData) {
        if (Objects.isNull(carData)) {
            return null;
        }
        Timestamp startDateValue = new Timestamp(carData.getStartDate().getTime());
        String start = startDateValue.toString();
        
        Timestamp endDateValue = new Timestamp(carData.getEndDate().getTime());
        String end = endDateValue.toString();
        TimeSlotRenting carDataDTO = new TimeSlotRenting();
        carDataDTO.setId(carData.getId());
        carDataDTO.setStartDate(start);
        carDataDTO.setEndDate(end);
        carDataDTO.setStatus(carData.getStatus());
        return carDataDTO;
    }
    
    public static List<TimeSlotRenting> convertTimeSlotDataToListTimeSlotDataDTOs(List<TimeSlot> temperatureDatas) {
        if (Objects.isNull(temperatureDatas)) {
            return null;
        }
        List<TimeSlotRenting> currentTemperatureList = new ArrayList<>();
        temperatureDatas.forEach(temperatureData -> {
            currentTemperatureList.add(convertTimeSlotDataToTimeSlotDataDTO(temperatureData));
        });
        return currentTemperatureList;
    }
    
    /**
     * Convert timeslot, car data
     */
    private static SearchCar convertDataToDataDTO(TimeSlot timeSlotData) {
        if (Objects.isNull(timeSlotData)) {
            return null;
        }
        SearchCar carDataDTO = new SearchCar();
        carDataDTO.setCarID(timeSlotData.getCar().getIdCar());
        carDataDTO.setStatus(timeSlotData.getStatus());
        return carDataDTO;
    }
    
    public static List<SearchCar> convertDataToListDataDTOs(List<TimeSlot> temperatureDatas) {
        if (Objects.isNull(temperatureDatas)) {
            return null;
        }
        List<SearchCar> currentTemperatureList = new ArrayList<>();
        temperatureDatas.forEach(temperatureData -> {
            currentTemperatureList.add(convertDataToDataDTO(temperatureData));
        });
        return currentTemperatureList;
    }
    
    /**
     * Convert search and view car details with time-slot, car, owner account and personal info
     */
    private static ReturnResult convertDataReturnToDataReturnDTO(TimeSlot timeSlotData) {
    	if (Objects.isNull(timeSlotData)) {
    		return null;
    	}
    	ReturnResult returnDataDTO = new ReturnResult();
    	
    	returnDataDTO.setCarId(timeSlotData.getCar().getIdCar());
    	returnDataDTO.setCarLicense(timeSlotData.getCar().getLicensePlate());
    	returnDataDTO.setDefaultPrice(timeSlotData.getCar().getDefaultPrice());
    	returnDataDTO.setDistanceLimit(timeSlotData.getCar().getDistanceLimit());
    	returnDataDTO.setOverDistancePrice(timeSlotData.getCar().getOverDistancePrice());
    	returnDataDTO.setSeat(timeSlotData.getCar().getSeat());
    	returnDataDTO.setManufacturedYear(timeSlotData.getCar().getManufacturedYear());

    	returnDataDTO.setOwnerId(timeSlotData.getCar().getOwner().getAccount().getIdAccount());
    	returnDataDTO.setOwnerFullName(timeSlotData.getCar().getOwner().getAccount().getPersonalInformation().getFullName());
    	returnDataDTO.setOwnerPhoneNumber(timeSlotData.getCar().getOwner().getAccount().getPersonalInformation().getPhoneNumber());
    	return returnDataDTO;
    }
    
    public static List<ReturnResult> convertDataReturnToListDataReturnDTOs(List<TimeSlot> temperatureDatas) {
    	if (Objects.isNull(temperatureDatas)) {
    		return null;
    	}
    	List<ReturnResult> currentData = new ArrayList<>();
    	temperatureDatas.forEach(tempData -> {
    		currentData.add(convertDataReturnToDataReturnDTO(tempData));
    	});
    	return currentData;
    }
    
    /**
     * Convert data of contract - only selected contract only
     */
    private static ContractResultById convertContractDataToContractDataDTO(Contract contractData) {
    	if (Objects.isNull(contractData)) {
    		return null;
    	}
    	ContractResultById contractDataDTO = new ContractResultById();
    	contractDataDTO.setStartDate(contractData.getStartDate());
    	contractDataDTO.setEndDate(contractData.getEndDate());
    	contractDataDTO.setAdvanceMoney(contractData.getAdvanceMoney());
    	contractDataDTO.setNegotiatePrice(contractData.getNegotiatePrice());
    	contractDataDTO.setCreateDate(contractData.getCreateDate());
    	return contractDataDTO;
    }
    
    public static List<ContractResultById> convertContractDataToListContractDataDTOs(List<Contract> contractDatas) {
    	if (Objects.isNull(contractDatas)) {
    		return null;
    	}
    	List<ContractResultById> currentData = new ArrayList<>();
    	contractDatas.forEach(tempData -> {
    		currentData.add(convertContractDataToContractDataDTO(tempData));
    	});
    	return currentData;
    }
}
