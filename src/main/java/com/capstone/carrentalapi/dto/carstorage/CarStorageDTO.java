package com.capstone.carrentalapi.dto.carstorage;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class CarStorageDTO {
	
	private Integer idOwner;

}
