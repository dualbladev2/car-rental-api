package com.capstone.carrentalapi.dto.carstorage;

import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class UpdateCarStorage {
	
	private String storageName;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private Timestamp lastUpdated;
    private Integer carNumber;

}
