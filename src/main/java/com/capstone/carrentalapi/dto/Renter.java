package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Collection;

@Data
public class Renter {
    private Integer idAccount;
    @JsonIgnore
    private Account account;
    private Collection<Review> reviews;

}
