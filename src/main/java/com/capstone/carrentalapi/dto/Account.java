package com.capstone.carrentalapi.dto;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Collection;

@Data
public class Account {
    private Integer idAccount;
    private String status;
    private Timestamp createdDate;
    private Timestamp lastUpdate;
    private String firebaseId;
    private Collection<AccountImage> accountImages;
    private Admin admin;
    private Owner owner;
    private PersonalInformation personalInformation;
    private Renter renter;

}
