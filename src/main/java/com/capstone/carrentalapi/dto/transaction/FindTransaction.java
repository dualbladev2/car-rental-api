package com.capstone.carrentalapi.dto.transaction;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class FindTransaction {
	
	private Integer transactionID;
	private String transactionDescription;
	private Integer advanceMoney;

}
