package com.capstone.carrentalapi.dto.carhavefeature;

import lombok.Data;

@Data
public class SimpleCarHaveFeature {
    private Integer idCarHaveFeature;
    private Integer idCar;
    private Integer idFeature;
}
