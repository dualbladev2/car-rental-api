package com.capstone.carrentalapi.dto.carmodel;

import com.capstone.carrentalapi.domain.model.CarBrand;
import com.capstone.carrentalapi.domain.model.CarCategory;
import com.capstone.carrentalapi.dto.Car;
import com.capstone.carrentalapi.domain.model.CarModel;
import com.capstone.carrentalapi.dto.carbrand.SimpleCarBrand;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import lombok.Data;

import java.util.Collection;

@Data
public class CarModelWithBrandAndCategory {
    private Integer idModel;
    private String name;
    private String description;
    private SimpleCarCategory carCategory;
    private SimpleCarBrand carBrand;

    public CarModelWithBrandAndCategory(Integer idModel, String name, String description, SimpleCarCategory carCategory, SimpleCarBrand carBrand) {
        this.idModel = idModel;
        this.name = name;
        this.description = description;
        this.carCategory = carCategory;
        this.carBrand = carBrand;
    }

    public static CarModelWithBrandAndCategory MapFromEntity(CarModel carModel){
        CarCategory category = carModel.getCarCategory();
        CarBrand brand = carModel.getCarBrand();
        return new CarModelWithBrandAndCategory(carModel.getIdModel(), carModel.getName(), carModel.getDescription(), new SimpleCarCategory(category.getIdCategory(), category.getName(), category.getDescription()), new SimpleCarBrand(brand.getIdBrand(), brand.getName(), brand.getDescription()));
    }
}
