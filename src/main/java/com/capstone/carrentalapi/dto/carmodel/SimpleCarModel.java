package com.capstone.carrentalapi.dto.carmodel;

import com.capstone.carrentalapi.dto.carbrand.SimpleCarBrand;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import lombok.Data;

@Data
public class SimpleCarModel {
    private Integer idModel;
    private Integer idCategory;
    private Integer idBrand;
    private String name;
    private String description;

    public SimpleCarModel(Integer idModel, Integer idCategory, Integer idBrand, String name, String description) {
        this.idModel = idModel;
        this.idCategory = idCategory;
        this.idBrand = idBrand;
        this.name = name;
        this.description = description;
    }
}
