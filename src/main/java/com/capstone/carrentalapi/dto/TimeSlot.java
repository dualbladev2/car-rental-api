package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class TimeSlot {
    private Integer id;
    private BigDecimal price;
    private String status;
    private Timestamp endDate;
    private Timestamp startDate;
    @JsonIgnore
    private Car car;
    private Integer carIdCar;
}
