package com.capstone.carrentalapi.dto;

import lombok.Data;

import java.util.Collection;

@Data
public class CarFeature {
    private Integer idFeature;
    private String name;
    private String description;
    private Collection<CarHaveFeature> carHaveFeatures;

}
