package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class Review {
    private Integer idReview;
    private BigDecimal rating;
    private String description;
    private Timestamp createdDate;
    private Timestamp lastUpdate;
    @JsonIgnore
    private Renter renter;
    @JsonIgnore
    private Car car;

    private Integer renterIdAccount;
    private Integer carIdCar;
}
