package com.capstone.carrentalapi.dto;

import lombok.Data;

import java.util.Collection;

@Data
public class CarCategory {
    private Integer idCategory;
    private String name;
    private String description;
    private Collection<CarModel> carModels;

}
