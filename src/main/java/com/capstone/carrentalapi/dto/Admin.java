package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Admin {
    private Integer idAccount;
    @JsonIgnore
    private Account account;


}
