package com.capstone.carrentalapi.dto.carbrand;

import com.capstone.carrentalapi.domain.model.CarBrand;
import com.capstone.carrentalapi.domain.model.CarCategory;
import com.capstone.carrentalapi.dto.CarModel;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import lombok.Data;

import java.util.Collection;

@Data
public class SimpleCarBrand {
    private Integer idBrand;
    private String name;
    private String description;

    public SimpleCarBrand(Integer idBrand, String name, String description) {
        this.idBrand = idBrand;
        this.name = name;
        this.description = description;
    }

    public static SimpleCarBrand MapFromEntity(CarBrand carBrand){
        return new SimpleCarBrand(carBrand.getIdBrand(), carBrand.getName(), carBrand.getDescription());
    }
}
