package com.capstone.carrentalapi.dto.carbrand;

import lombok.Data;

@Data
public class CreateCarBrandRequest {
    private String name;
    private String description;

    public CreateCarBrandRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
