package com.capstone.carrentalapi.dto.timeslot;

import java.math.BigDecimal;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ReturnResult {
	
	/*
	 * For car objects
	 */
	private Integer carId;
	private String carLicense;
	private BigDecimal defaultPrice;
	private Integer distanceLimit;
	private BigDecimal overDistancePrice;
	private Integer seat;
	private Short manufacturedYear;
	/*
	 * For Owner personal information
	 */
	private Integer ownerId;
	private String ownerFullName;
	private String ownerPhoneNumber;
	
}
