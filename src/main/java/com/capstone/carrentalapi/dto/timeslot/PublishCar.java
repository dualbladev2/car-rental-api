package com.capstone.carrentalapi.dto.timeslot;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class PublishCar {
	
	private Integer idCar;
	private BigDecimal price;
	private Date startDate;
	private Date endDate;

}
