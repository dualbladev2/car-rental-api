package com.capstone.carrentalapi.dto.timeslot;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class SearchCar {
	
	private String status;
	private Integer carID;

}
