package com.capstone.carrentalapi.dto.timeslot;
 
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class TimeSlotRenting {
	
	private Integer id;
	private String status;
	private String startDate;
	private String endDate;

}
