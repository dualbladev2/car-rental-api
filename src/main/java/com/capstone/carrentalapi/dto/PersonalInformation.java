package com.capstone.carrentalapi.dto;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class PersonalInformation {
    private Integer idAccount;
    private Byte gender;
    private String identityCard;
    private String drivingLicense;
    private String phoneNumber;
    private String fullName;
    private Timestamp createdDate;
    private Timestamp lastUpdate;
    private String email;
    private String address;
    private String birthDate;

}
