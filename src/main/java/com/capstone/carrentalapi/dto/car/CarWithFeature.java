package com.capstone.carrentalapi.dto.car;

import com.capstone.carrentalapi.dto.carfeature.SimpleCarFeature;
import com.capstone.carrentalapi.dto.owner.SimpleOwner;
import lombok.Data;

import java.util.Collection;

@Data
public class CarWithFeature {
    private Integer idCar;
    private Collection<SimpleCarFeature> features;


}
