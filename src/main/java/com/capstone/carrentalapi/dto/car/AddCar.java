package com.capstone.carrentalapi.dto.car;

import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class AddCar {
	
	private Integer idCar;
    private String licensePlate;
    private BigDecimal defaultPrice;
    private Integer distanceLimit;
    private BigDecimal overDistancePrice;
    private Integer seat;
    private Short manufacturedYear;
    private String registrationNumber;
    private String description;
    private BigDecimal publicLatitude;
    private BigDecimal publicLongitude;
    private String rentingTerm;
    private Integer fuelConsumption;
    private Integer idCarStorage;
    private Integer idOwner;
    private Integer idCarModel;

}
