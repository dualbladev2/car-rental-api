package com.capstone.carrentalapi.dto.car;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class CarLocation {
    private Integer idLocation;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private Timestamp date;
}
