package com.capstone.carrentalapi.dto.car;

import lombok.Data;

@Data
public class CarVerify {
    private Integer idCar;
    private String licensePlate;
}
