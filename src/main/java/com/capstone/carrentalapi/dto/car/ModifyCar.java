package com.capstone.carrentalapi.dto.car;

import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ModifyCar {
	
	private Integer idCar;

}
