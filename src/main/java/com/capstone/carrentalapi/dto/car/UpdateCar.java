package com.capstone.carrentalapi.dto.car;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.capstone.carrentalapi.domain.enums.Status;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class UpdateCar {
	
	private Status status;//
    private String licensePlate;
    private BigDecimal defaultPrice;//
    private Integer distanceLimit;//
    private BigDecimal overDistancePrice;//
    private Integer seat;//
    private BigDecimal publicLatitude;
    private BigDecimal publicLongitude;
    private String description;
    private String rentingTerm;
    private Integer fuelConsumption;
    private Integer completedTrip;
    private Timestamp lastUpdated;
	
}
