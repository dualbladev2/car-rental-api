package com.capstone.carrentalapi.dto.car;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class LicensePlateCar {
	
	private String licensePlate;

}
