package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Collection;


@Data
public class Owner {
    private Integer idAccount;
    private Collection<Car> cars;
    private Collection<CarStorage> carStorages;
    @JsonIgnore
    private Account account;
}
