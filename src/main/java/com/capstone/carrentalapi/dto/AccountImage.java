package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class AccountImage {
    private Integer idAccountImage;
    private String imageType;
    private String url;
    private Timestamp createdDate;
    private Byte isActive;
    @JsonIgnore
    private Account account;

}
