package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class CarHaveFeature {
    private Integer idCarHaveFeature;
    @JsonIgnore
    private Car car;
    @JsonIgnore
    private CarFeature carFeature;

}
