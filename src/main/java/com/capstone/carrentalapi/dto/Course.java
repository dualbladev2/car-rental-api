package com.capstone.carrentalapi.dto;

import lombok.Data;

@Data
public class Course {
    private Integer id;
    private String name;
}
