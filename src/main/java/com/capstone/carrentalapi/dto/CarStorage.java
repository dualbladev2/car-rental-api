package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;

@Data
public class CarStorage {
    private Integer idStorage;
    private String name;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private Integer carNumber;
    private Timestamp createdDate;
    private Timestamp lastUpdated;
    private Collection<Car> cars;
    @JsonIgnore
    private Owner owner;

}
