package com.capstone.carrentalapi.dto;

import com.capstone.carrentalapi.domain.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;

@Data
public class Car {
    private Integer idCar;
    private Status status;
    private String licensePlate;
    private BigDecimal defaultPrice;
    private Integer distanceLimit;
    private BigDecimal overDistancePrice;
    private Integer seat;
    private Short manufacturedYear;
    private String registrationNumber;
    private BigDecimal publicLatitude;
    private BigDecimal publicLongitude;
    private BigDecimal recentRating;
    private BigDecimal totalRating;
    private String description;
    private String rentingTerm;
    private Integer fuelConsumption;
    private Integer completedTrip;
    private Timestamp createdDate;
    private Timestamp lastUpdated;

    @JsonIgnore
    private CarStorage carStorage;
    @JsonIgnore
    private Owner owner;
    @JsonIgnore
    private CarModel carModel;

    private Integer carStorageIdAccount;
    private Integer ownerIdAccount;
    private Integer carModelIdAccount;

    private Collection<CarHaveFeature> carHaveFeatures;
    private Collection<CarImage> carImages;
    private Collection<Location> locations;
    private Collection<Review> reviews;
    private Collection<TimeSlot> timeSlots;

}
