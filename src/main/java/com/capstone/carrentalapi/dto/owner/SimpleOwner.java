package com.capstone.carrentalapi.dto.owner;

public class SimpleOwner {
    private Integer idAccount;
    private String email;
    private String fullName;
    private Byte gender;
}
