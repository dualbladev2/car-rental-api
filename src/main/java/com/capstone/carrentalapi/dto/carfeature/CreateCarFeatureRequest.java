package com.capstone.carrentalapi.dto.carfeature;

import lombok.Data;

@Data
public class CreateCarFeatureRequest {
    private String name;
    private String description;

    public CreateCarFeatureRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
