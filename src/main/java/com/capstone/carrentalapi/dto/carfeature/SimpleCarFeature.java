package com.capstone.carrentalapi.dto.carfeature;

import com.capstone.carrentalapi.domain.model.CarFeature;
import lombok.Data;

@Data
public class SimpleCarFeature {
    private Integer idFeature;
    private String name;
    private String description;

    public SimpleCarFeature(Integer idFeature, String name, String description) {
        this.idFeature = idFeature;
        this.name = name;
        this.description = description;
    }

    public static SimpleCarFeature MapFromEntity(CarFeature carFeature){
        return new SimpleCarFeature(carFeature.getIdFeature(), carFeature.getName(), carFeature.getDescription());
    }
}
