package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class Location {
    private Integer idLocation;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private Timestamp date;
    @JsonIgnore
    private Car car;

}
