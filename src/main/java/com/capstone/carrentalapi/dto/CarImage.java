package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class CarImage {
    private Integer idCarImage;
    private String imageType;
    private String url;
    private Timestamp createdDate;
    private Byte isActive;
    @JsonIgnore
    private Car car;

    private Integer carIdCar;
}
