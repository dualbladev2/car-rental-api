package com.capstone.carrentalapi.dto;

import lombok.Data;

import java.util.Collection;

@Data
public class CarBrand {
    private Integer idBrand;
    private String name;
    private String description;
    private Collection<CarModel> carModels;

}
