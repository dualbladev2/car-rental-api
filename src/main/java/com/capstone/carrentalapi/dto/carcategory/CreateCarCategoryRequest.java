package com.capstone.carrentalapi.dto.carcategory;

import lombok.Data;

@Data
public class CreateCarCategoryRequest {
    private String name;
    private String description;

    public CreateCarCategoryRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
