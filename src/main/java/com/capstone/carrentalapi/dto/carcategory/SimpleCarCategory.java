package com.capstone.carrentalapi.dto.carcategory;

import com.capstone.carrentalapi.domain.model.CarCategory;
import com.capstone.carrentalapi.domain.model.CarFeature;
import com.capstone.carrentalapi.dto.carfeature.SimpleCarFeature;
import lombok.Data;

@Data
public class SimpleCarCategory {
    private Integer idCategory;
    private String name;
    private String description;

    public SimpleCarCategory(Integer idCategory, String name, String description) {
        this.idCategory = idCategory;
        this.name = name;
        this.description = description;
    }

    public static SimpleCarCategory MapFromEntity(CarCategory carCategory){
        return new SimpleCarCategory(carCategory.getIdCategory(), carCategory.getName(), carCategory.getDescription());
    }
}
