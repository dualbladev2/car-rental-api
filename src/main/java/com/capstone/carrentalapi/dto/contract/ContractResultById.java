package com.capstone.carrentalapi.dto.contract;

import java.sql.Timestamp;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ContractResultById {
	
	private Timestamp startDate;
	private Timestamp endDate;
	private Integer advanceMoney;
	private Integer negotiatePrice;
	private Timestamp createDate;

}
