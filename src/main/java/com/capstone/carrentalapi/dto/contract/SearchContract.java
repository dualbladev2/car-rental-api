package com.capstone.carrentalapi.dto.contract;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class SearchContract {
	
	private String dateInput;

}
