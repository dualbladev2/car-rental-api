package com.capstone.carrentalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Collection;

@Data
public class CarModel {
    private Integer idModel;
    private String name;
    private String description;
    private Collection<Car> cars;
    @JsonIgnore
    private CarCategory carCategory;
    @JsonIgnore
    private CarBrand carBrand;

}
