package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.model.Renter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RenterRepository extends JpaRepository<Renter, Integer> {
}
