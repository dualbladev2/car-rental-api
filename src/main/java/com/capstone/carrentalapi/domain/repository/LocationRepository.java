package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.model.Location;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface LocationRepository extends PagingAndSortingRepository<Location, Integer> {
    List<Location> findAllByCar_IdCar(Integer idCar, Pageable pageable);
}
