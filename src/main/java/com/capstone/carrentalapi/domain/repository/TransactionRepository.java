package com.capstone.carrentalapi.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capstone.carrentalapi.domain.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
	
	Transaction findByIdTransaction(Integer transactionID);

}
