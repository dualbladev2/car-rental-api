package com.capstone.carrentalapi.domain.repository;


import com.capstone.carrentalapi.domain.model.CarCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarCategoryRepository extends JpaRepository<CarCategory, Integer> {
}
