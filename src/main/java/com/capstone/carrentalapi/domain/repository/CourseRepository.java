package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.model.Course;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course,Integer> {
	
	List<Course> findByName(String courseName);
	
}
