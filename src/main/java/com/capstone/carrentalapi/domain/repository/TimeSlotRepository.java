package com.capstone.carrentalapi.domain.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capstone.carrentalapi.domain.model.TimeSlot;


@Repository
public interface TimeSlotRepository extends JpaRepository<TimeSlot, Integer> {
	
	//List<TimeSlot> findByStartDateAndEndDateAndStatus(Timestamp startDate, Timestamp endDate, String status);

	List<TimeSlot> findByStatus(String status);
	
	List<TimeSlot> findByStartDateBetween(Timestamp startDate, Timestamp startDateEnd);
	
	List<TimeSlot> findByStatusAndCarIdCar(String carStatus, Integer carID);
}
