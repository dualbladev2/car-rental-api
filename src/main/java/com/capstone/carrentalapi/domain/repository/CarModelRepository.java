package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.enums.Status;
import com.capstone.carrentalapi.domain.model.Account;
import com.capstone.carrentalapi.domain.model.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CarModelRepository extends JpaRepository<CarModel, Integer> {
    @Query("SELECT a FROM CarModel a WHERE a.carBrand.idBrand = ?1 AND a.carCategory.idCategory = ?2")
    List<CarModel> findByBrandAndCategory(Integer idBrand, Integer idCategory);
}
