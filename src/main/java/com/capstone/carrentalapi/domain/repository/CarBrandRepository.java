package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.model.CarBrand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarBrandRepository extends JpaRepository<CarBrand, Integer> {
}
