package com.capstone.carrentalapi.domain.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capstone.carrentalapi.domain.model.Contract;

@Repository
public interface ContractRepository extends JpaRepository<Contract, Integer> {
	
	List<Contract> findByCreateDate(Timestamp searchDate);
	
	List<Contract> findByIdContract(Integer contractID);

}
