package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.enums.Status;
import com.capstone.carrentalapi.domain.model.Car;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarRepository extends JpaRepository<Car, Integer> {
	
	void deleteByIdCar(Integer carId);
	
	Car findByLicensePlate(String licensePlate);
	
	List<Car> findByStatus(String carStatus);

	List<Car> findByStatus(Status status);
}
