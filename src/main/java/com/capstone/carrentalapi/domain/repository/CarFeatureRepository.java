package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.model.CarFeature;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarFeatureRepository extends JpaRepository<CarFeature, Integer> {
}
