package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.enums.Status;
import com.capstone.carrentalapi.domain.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account getAccountByFirebaseId(String firebaseId);

    @Query("SELECT a FROM Account a INNER JOIN Renter r ON a.idAccount = r.idAccount WHERE a.status = :status")
    List<Account> findAccountRenterByStatus(@Param("status") Status status);
}
