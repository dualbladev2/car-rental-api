package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.model.PersonalInformation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalInformationRepository extends JpaRepository<PersonalInformation, Integer> {
}
