package com.capstone.carrentalapi.domain.repository;

import com.capstone.carrentalapi.domain.model.Owner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OwnerRepository extends JpaRepository<Owner, Integer> {
}
