package com.capstone.carrentalapi.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capstone.carrentalapi.domain.model.CarStorage;

@Repository
public interface CarStorageRepository extends JpaRepository<CarStorage, Integer> {
	
	List<CarStorage> findByOwnerIdAccount(Integer id);
	
	void deleteByName(String name);
	
	CarStorage findByName(String name);
	
}
