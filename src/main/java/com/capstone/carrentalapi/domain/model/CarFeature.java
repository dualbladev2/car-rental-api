package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
@Table(name = "car_feature", schema = "car-rental-db")
public class CarFeature {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_feature", nullable = false)
    private Integer idFeature;
    @Basic
    @Column(name = "name", length = 20)
    private String name;
    @Basic
    @Column(name = "description", nullable = true, length = 100)
    private String description;
    @OneToMany(mappedBy = "carFeature")
    private Collection<CarHaveFeature> carHaveFeatures;

}
