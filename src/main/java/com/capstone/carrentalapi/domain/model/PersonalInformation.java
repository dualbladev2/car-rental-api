package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "personal_information", schema = "car-rental-db")
public class PersonalInformation {
    @Id
    @Column(name = "id_account", nullable = false)
    private Integer idAccount;
    @Basic
    @Column(name = "gender", nullable = true)
    private Byte gender;
    @Basic
    @Column(name = "identity_card", nullable = true, length = 9)
    private String identityCard;
    @Basic
    @Column(name = "driving_license", nullable = true, length = 12)
    private String drivingLicense;
    @Basic
    @Column(name = "phone_number", nullable = true, length = 12)
    private String phoneNumber;
    @Basic
    @Column(name = "full_name", nullable = true, length = 50)
    private String fullName;
    @Basic
    @Column(name = "created_date")
    private Timestamp createdDate;
    @Basic
    @Column(name = "last_update")
    private Timestamp lastUpdate;
    @Basic
    @Column(name = "email", nullable = true, length = 45)
    private String email;
    @Basic
    @Column(name = "address", nullable = true, length = 45)
    private String address;
    @Basic
    @Column(name = "birth_date", nullable = true, length = 45)
    private Date birthDate;
    @OneToOne
    @JoinColumn(name = "id_account", referencedColumnName = "id_account")
    private Account account;

}
