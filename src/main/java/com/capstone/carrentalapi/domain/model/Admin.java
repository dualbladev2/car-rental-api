package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Admin {
    @Id
    @Column(name = "id_account", nullable = false)
    private Integer idAccount;
    @OneToOne
    @JoinColumn(name = "id_account", referencedColumnName = "id_account")
    private Account account;


}
