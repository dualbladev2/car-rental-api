package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
@Data
@Entity
@Table(name = "account_image", schema = "car-rental-db")
public class AccountImage {
    @Id
    @Column(name = "id_account_image", nullable = false)
    private Integer idAccountImage;
    @Basic
    @Column(name = "image_type", length = 20)
    private String imageType;
    @Basic
    @Column(name = "url", length = 255)
    private String url;
    @Basic
    @Column(name = "created_date")
    private Timestamp createdDate;
    @Basic
    @Column(name = "is_active")
    private Byte isActive;
    @ManyToOne
    @JoinColumn(name = "id_account", referencedColumnName = "id_account")
    private Account account;

}
