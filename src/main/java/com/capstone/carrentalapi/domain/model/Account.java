package com.capstone.carrentalapi.domain.model;

import com.capstone.carrentalapi.domain.enums.Status;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Data
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_account", nullable = false)
    private Integer idAccount;

    @Basic
    @Column(name = "status", length = 20)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Basic
    @Column(name = "created_date")
    private Timestamp createdDate;

    @Basic
    @Column(name = "last_update")
    private Timestamp lastUpdate;

    @Basic
    @Column(name = "firebase_id", nullable = true, length = 128)
    private String firebaseId;

    @OneToMany(mappedBy = "account")
    private Collection<AccountImage> accountImages;

    @OneToOne(mappedBy = "account")
    private Admin admin;

    @OneToOne(mappedBy = "account")
    private Owner owner;

    @OneToOne(mappedBy = "account")
    private PersonalInformation personalInformation;

    @OneToOne(mappedBy = "account")
    private Renter renter;

}
