package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "time_slot", schema = "car-rental-db")
public class TimeSlot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic
    @Column(name = "price", precision = 4)
    private BigDecimal price;
    @Basic
    @Column(name = "status", length = 20)
    private String status;
    @Basic
    @Column(name = "end_date")
    private Timestamp endDate;
    @Basic
    @Column(name = "start_date")
    private Timestamp startDate;
    @ManyToOne
    @JoinColumn(name = "id_car", referencedColumnName = "id_car")
    private Car car;

}
