package com.capstone.carrentalapi.domain.model;

import com.capstone.carrentalapi.domain.enums.Status;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;

@Data
@Entity
@Table (name = "Car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_car", nullable = false)
    private Integer idCar;
    @Basic
    @Column(name = "status", length = 20)
    @Enumerated(EnumType.STRING)
    private Status status;
    @Basic
    @Column(name = "license_plate", length = 20)
    private String licensePlate;
    @Basic
    @Column(name = "default_price", precision = 4)
    private BigDecimal defaultPrice;
    @Basic
    @Column(name = "distance_limit")
    private Integer distanceLimit;
    @Basic
    @Column(name = "over_distance_price", precision = 4)
    private BigDecimal overDistancePrice;
    @Basic
    @Column(name = "seat")
    private Integer seat;
    @Basic
    @Column(name = "manufactured_year")
    private Short manufacturedYear;
    @Basic
    @Column(name = "registration_number", length = 20)
    private String registrationNumber;
    @Basic
    @Column(name = "public_latitude", precision = 8)
    private BigDecimal publicLatitude;
    @Basic
    @Column(name = "public_longitude", precision = 8)
    private BigDecimal publicLongitude;
    @Basic
    @Column(name = "recent_rating", precision = 1)
    private BigDecimal recentRating;
    @Basic
    @Column(name = "total_rating", precision = 1)
    private BigDecimal totalRating;
    @Basic
    @Column(name = "description", length = -1)
    private String description;
    @Basic
    @Column(name = "renting_term", length = -1)
    private String rentingTerm;
    @Basic
    @Column(name = "fuel_consumption")
    private Integer fuelConsumption;
    @Basic
    @Column(name = "completed_trip")
    private Integer completedTrip;
    @Basic
    @Column(name = "created_date")
    private Timestamp createdDate;
    @Basic
    @Column(name = "last_updated")
    private Timestamp lastUpdated;
    @ManyToOne
    @JoinColumn(name = "id_storage", referencedColumnName = "id_storage")
    private CarStorage carStorage;
    @ManyToOne
    @JoinColumn(name = "id_owner", referencedColumnName = "id_account")
    private Owner owner;
    @ManyToOne
    @JoinColumn(name = "id_model", referencedColumnName = "id_model")
    private CarModel carModel;
    @OneToMany(mappedBy = "car")
    private Collection<CarHaveFeature> carHaveFeatures;
    @OneToMany(mappedBy = "car")
    private Collection<CarImage> carImages;
    @OneToMany(mappedBy = "car")
    private Collection<Location> locations;
    @OneToMany(mappedBy = "car")
    private Collection<Review> reviews;
    @OneToMany(mappedBy = "car")
    private Collection<TimeSlot> timeSlots;

}
