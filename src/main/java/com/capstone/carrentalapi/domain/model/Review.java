package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_review", nullable = false)
    private Integer idReview;
    @Basic
    @Column(name = "rating", precision = 1)
    private BigDecimal rating;
    @Basic
    @Column(name = "description", nullable = true, length = 255)
    private String description;
    @Basic
    @Column(name = "created_date")
    private Timestamp createdDate;
    @Basic
    @Column(name = "last_update")
    private Timestamp lastUpdate;
    @ManyToOne
    @JoinColumn(name = "id_renter", referencedColumnName = "id_account")
    private Renter renter;
    @ManyToOne
    @JoinColumn(name = "id_car", referencedColumnName = "id_car")
    private Car car;

}
