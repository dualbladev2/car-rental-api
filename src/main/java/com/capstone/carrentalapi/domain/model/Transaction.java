package com.capstone.carrentalapi.domain.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
public class Transaction {
    @Id
    @Column(name = "id_transaction", nullable = false)
    private Integer idTransaction;
    @Basic
    @Column(name = "amount", nullable = true)
    private Integer amount;
    @Basic
    @Column(name = "create_date", nullable = true)
    private Timestamp createDate;
    @Basic
    @Column(name = "id_account", nullable = true)
    private Integer idAccount;
    @Basic
    @Column(name = "description", nullable = true, length = 45)
    private String description;
    @ManyToOne
    @JoinColumn(name = "id_contract", referencedColumnName = "id_contract")
    private Contract contract;

}
