package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
public class Owner {
    @Id@Column(name = "id_account", nullable = false)
    private Integer idAccount;
    @OneToMany(mappedBy = "owner")
    private Collection<Car> cars;
    @OneToMany(mappedBy = "owner")
    private Collection<CarStorage> carStorages;
    @OneToOne@JoinColumn(name = "id_account", referencedColumnName = "id_account")
    private Account account;

}
