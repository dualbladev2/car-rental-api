package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
@Table(name = "car_category", schema = "car-rental-db")
public class CarCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_category", nullable = false)
    private Integer idCategory;
    @Basic
    @Column(name = "name", length = 20)
    private String name;
    @Basic
    @Column(name = "description", nullable = true, length = 100)
    private String description;
    @OneToMany(mappedBy = "carCategory")
    private Collection<CarModel> carModels;

}
