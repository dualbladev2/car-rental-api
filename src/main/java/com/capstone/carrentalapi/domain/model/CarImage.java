package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "car_image", schema = "car-rental-db")
public class CarImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_car_image", nullable = false)
    private Integer idCarImage;
    @Basic
    @Column(name = "image_type", length = 20)
    private String imageType;
    @Basic
    @Column(name = "url", length = 255)
    private String url;
    @Basic
    @Column(name = "created_date")
    private Timestamp createdDate;
    @Basic
    @Column(name = "is_active")
    private Byte isActive;
    @ManyToOne
    @JoinColumn(name = "id_car", referencedColumnName = "id_car")
    private Car car;

}
