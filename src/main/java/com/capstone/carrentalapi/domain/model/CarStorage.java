package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;

@Data
@Entity
@Table(name = "car_storage", schema = "car-rental-db")
public class CarStorage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_storage", nullable = false)
    private Integer idStorage;
    @Basic
    @Column(name = "name", length = 50)
    private String name;
    @Basic
    @Column(name = "latitude", precision = 8)
    private BigDecimal latitude;
    @Basic
    @Column(name = "longitude", precision = 8)
    private BigDecimal longitude;
    @Basic
    @Column(name = "car_number")
    private Integer carNumber;
    @Basic
    @Column(name = "created_date")
    private Timestamp createdDate;
    @Basic
    @Column(name = "last_updated")
    private Timestamp lastUpdated;
    @OneToMany(mappedBy = "carStorage")
    private Collection<Car> cars;
    @ManyToOne
    @JoinColumn(name = "id_owner", referencedColumnName = "id_account")
    private Owner owner;

}
