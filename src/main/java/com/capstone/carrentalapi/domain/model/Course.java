package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "course", schema = "car-rental-db", catalog = "")
public class Course {
    @Id@Column(name = "id", nullable = false)
    private Integer id;
    @Basic@Column(name = "name", nullable = true, length = 45)
    private String name;

}
