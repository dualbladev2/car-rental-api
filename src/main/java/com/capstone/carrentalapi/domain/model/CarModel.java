package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
@Table(name = "car_model", schema = "car-rental-db")
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_model", nullable = false)
    private Integer idModel;
    @Basic
    @Column(name = "name", length = 20)
    private String name;
    @Basic
    @Column(name = "description", nullable = true, length = 100)
    private String description;
    @OneToMany(mappedBy = "carModel")
    private Collection<Car> cars;
    @ManyToOne
    @JoinColumn(name = "id_category", referencedColumnName = "id_category")
    private CarCategory carCategory;
    @ManyToOne
    @JoinColumn(name = "id_brand", referencedColumnName = "id_brand")
    private CarBrand carBrand;

}
