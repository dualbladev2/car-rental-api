package com.capstone.carrentalapi.domain.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Data
@Entity
public class Contract {
    @Id
    @Column(name = "id_contract", nullable = false)
    private Integer idContract;
    @Basic
    @Column(name = "start_date", nullable = true)
    private Timestamp startDate;
    @Basic
    @Column(name = "end_date", nullable = true)
    private Timestamp endDate;
    @Basic
    @Column(name = "advance_money", nullable = true)
    private Integer advanceMoney;
    @Basic
    @Column(name = "negotiate_price", nullable = true)
    private Integer negotiatePrice;
    @Basic
    @Column(name = "create_date", nullable = true)
    private Timestamp createDate;
    @OneToMany(mappedBy = "contract")
    private Collection<Transaction> transactions;

}
