package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
public class Renter {
    @Id
    @Column(name = "id_account", nullable = false)
    private Integer idAccount;
    @OneToOne
    @JoinColumn(name = "id_account", referencedColumnName = "id_account")
    private Account account;
    @OneToMany(mappedBy = "renter")
    private Collection<Review> reviews;

}
