package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "car_have_feature", schema = "car-rental-db", catalog = "")
public class CarHaveFeature {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_car_have_feature", nullable = false)
    private Integer idCarHaveFeature;
    @ManyToOne
    @JoinColumn(name = "id_car", referencedColumnName = "id_car")
    private Car car;
    @ManyToOne
    @JoinColumn(name = "id_feature", referencedColumnName = "id_feature")
    private CarFeature carFeature;

}
