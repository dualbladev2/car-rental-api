package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
@Table(name = "car_brand", schema = "car-rental-db")
public class CarBrand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_brand", nullable = false)
    private Integer idBrand;
    @Basic@Column(name = "name", length = 20)
    private String name;
    @Basic@Column(name = "description", nullable = true, length = 100)
    private String description;
    @OneToMany(mappedBy = "carBrand")
    private Collection<CarModel> carModels;

}
