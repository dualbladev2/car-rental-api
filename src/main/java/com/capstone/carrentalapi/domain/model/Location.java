package com.capstone.carrentalapi.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_location")
    private Integer idLocation;
    @Basic
    @Column(name = "latitude", precision = 8)
    private BigDecimal latitude;
    @Basic
    @Column(name = "longitude", precision = 8)
    private BigDecimal longitude;
    @Basic
    @Column(name = "date")
    private Timestamp date;
    @ManyToOne
    @JoinColumn(name = "id_car", referencedColumnName = "id_car")
    private Car car;

}
