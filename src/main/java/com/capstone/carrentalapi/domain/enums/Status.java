package com.capstone.carrentalapi.domain.enums;

public enum Status {
    PENDING, BLOCKED, REJECTED, UNVERIFIED, VERIFIED;
}
