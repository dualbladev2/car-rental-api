package com.capstone.carrentalapi.domain.enums;

public enum CarRentStatus {
	AVAILABLE, NOT_AVAILABLE
}
