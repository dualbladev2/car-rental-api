package com.capstone.carrentalapi.rest;

import java.text.ParseException;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capstone.carrentalapi.domain.model.Contract;
import com.capstone.carrentalapi.dto.contract.ContractResultById;
import com.capstone.carrentalapi.dto.contract.SearchContract;
import com.capstone.carrentalapi.dto.contract.SearchContractById;
import com.capstone.carrentalapi.service.ContractService;

@RestController
@RequestMapping("/contract")
public class ContractController {
	
	private ContractService contractService;
	
	private ModelMapper modelMapper;
	
	@Autowired
	public ContractController(ContractService contractService, ModelMapper modelMapper) {
		this.contractService = contractService;
		this.modelMapper = modelMapper;
	}
	
	@PostMapping(value="/search-by-date-input")
	public List<Contract> searchByDateInput(SearchContract searchContract) throws ParseException {
		return contractService.searchContractByDate(searchContract);
	}
	
	@PostMapping(value="/find-by-contract-id")
	public List<ContractResultById> findByContractId(SearchContractById contractID) {
		return contractService.findContractById(contractID);
	}

}
