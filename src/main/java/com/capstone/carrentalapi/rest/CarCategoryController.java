package com.capstone.carrentalapi.rest;

import com.capstone.carrentalapi.dto.carbrand.SimpleCarBrand;
import com.capstone.carrentalapi.dto.carcategory.CreateCarCategoryRequest;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import com.capstone.carrentalapi.dto.carfeature.CreateCarFeatureRequest;
import com.capstone.carrentalapi.dto.carfeature.SimpleCarFeature;
import com.capstone.carrentalapi.service.CarCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/carCategories")
public class CarCategoryController {

    private CarCategoryService carCategoryService;

    @Autowired
    public CarCategoryController(CarCategoryService carCategoryService) {
        this.carCategoryService = carCategoryService;
    }

    @GetMapping
    public Collection<SimpleCarCategory> FindAll(){
        return carCategoryService.FindAll();
    }

    @GetMapping("/FindById")
    public SimpleCarCategory FindById(Integer idCategory){
        return carCategoryService.FindById(idCategory);
    }

    @PostMapping
    public SimpleCarCategory CreateCarCategory(CreateCarCategoryRequest request){
        return carCategoryService.CreateCarCategory(request);
    }

    @PutMapping
    public SimpleCarCategory UpdateCarFeature(SimpleCarCategory request){
        return carCategoryService.UpdateCarCategory(request);
    }
}
