package com.capstone.carrentalapi.rest;

import com.capstone.carrentalapi.dto.car.CarWithFeature;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import com.capstone.carrentalapi.dto.carfeature.CreateCarFeatureRequest;
import com.capstone.carrentalapi.dto.carfeature.SimpleCarFeature;
import com.capstone.carrentalapi.service.CarFeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/carFeatures")
public class CarFeatureController {
    private CarFeatureService carFeatureService;

    @Autowired
    public CarFeatureController(CarFeatureService carFeatureService) {
        this.carFeatureService = carFeatureService;
    }

    @GetMapping
    public Collection<SimpleCarFeature> FindAll(){
        return carFeatureService.FindAll();
    }

    @GetMapping("/FindById")
    public SimpleCarFeature FindById(Integer idFeature){
        return carFeatureService.FindById(idFeature);
    }

    @PostMapping
    public SimpleCarFeature CreateCarFeature(CreateCarFeatureRequest request){
        return carFeatureService.CreateCarFeature(request);
    }

    @PutMapping
    public SimpleCarFeature UpdateCarFeature(SimpleCarFeature request){
        return carFeatureService.UpdateCarFeature(request);
    }

    @PutMapping("/UpdateFeatureOfCar")
    public CarWithFeature UpdateFeatureOfCar(Integer idCar, Collection<Integer> idFeatures){
        return carFeatureService.UpdateFeatureOfCar(idCar,idFeatures);
    }
}
