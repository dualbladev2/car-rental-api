package com.capstone.carrentalapi.rest;

import com.capstone.carrentalapi.dto.carfeature.SimpleCarFeature;
import com.capstone.carrentalapi.dto.carmodel.CarModelWithBrandAndCategory;
import com.capstone.carrentalapi.dto.carmodel.SimpleCarModel;
import com.capstone.carrentalapi.service.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/carModels")
public class CarModelController {
    private CarModelService carModelService;

    @Autowired
    public CarModelController(CarModelService carModelService) {
        this.carModelService = carModelService;
    }

    @GetMapping
    public Collection<CarModelWithBrandAndCategory> FindAll(){
        return carModelService.FindAll();
    }

    @GetMapping("/FindById")
    public CarModelWithBrandAndCategory FindById(Integer idModel){
        return carModelService.FindById(idModel);
    }

    @GetMapping("/FindByBrandAndCategory")
    public CarModelWithBrandAndCategory FindById(Integer idBrand, Integer idCategory){
        return carModelService.FindByBrandAndCategory(idBrand,idCategory);
    }

    @PutMapping
    public CarModelWithBrandAndCategory UpdateCarModel(SimpleCarModel request){
        return carModelService.UpdateCarModel(request);
    }

    @PostMapping
    public CarModelWithBrandAndCategory createCarModel(String name, String description, Integer idCategory, Integer idBrand){
        return carModelService.CreateCarModel(name, description, idCategory, idBrand);
    }
}
