package com.capstone.carrentalapi.rest;

import com.capstone.carrentalapi.domain.model.Course;
import com.capstone.carrentalapi.service.CourseService;

import org.apache.coyote.http11.Http11AprProtocol;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/courses")
public class CourseController {
    private CourseService courseService;
    private final ModelMapper modelMapper;

    @Autowired
    public CourseController(CourseService courseService, ModelMapper modelMapper) {
        this.courseService = courseService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Course> findAll(){
       return Arrays.asList(modelMapper.map(courseService.findAll(), Course[].class));
    }
    
    @PostMapping
    public void addCourse(@RequestBody Course course) {
    	 courseService.addCourse(course);
    	 return;
    }
    
    
    @PostMapping(value = "/getCourseData")
    public List<Course> findCourse(@RequestBody Course course) {
    	 return courseService.findCourse(course);
    }
}
