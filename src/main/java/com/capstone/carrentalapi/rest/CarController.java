package com.capstone.carrentalapi.rest;

import com.capstone.carrentalapi.domain.enums.Status;
import com.capstone.carrentalapi.domain.model.Location;
import com.capstone.carrentalapi.dto.car.*;
import com.google.gson.JsonObject;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.capstone.carrentalapi.domain.model.Car;
import com.capstone.carrentalapi.service.CarService;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("/car")
public class CarController {
	
	private CarService carService;
	private final ModelMapper modelMapper;
	
	@Autowired
    public CarController(CarService carService, ModelMapper modelMapper) {
        this.carService = carService;
        this.modelMapper = modelMapper;
    }
	
	@PostMapping(value="/add-car")
	public void addCar(@RequestBody(required=false) AddCar car) {
		carService.addCar(car);
		return;
	}
	
	@PostMapping(value="/delete-car")
	public void deleteCar(@RequestBody ModifyCar car) {
		carService.deleteCar(car);
		return;
	}
	
	@PostMapping(value="/find-car")
	public Car findCarByLicensePlate(@RequestBody LicensePlateCar carLicensePlate) {
		return carService.findCarByLicensePlate(carLicensePlate);
	}
	
	@PostMapping(value="/update-car")
	public void updateCarByLicensePlate(@RequestBody UpdateCar carUpdate) {
		carService.updateCarByLicensePlate(carUpdate);
		return;
	}
	
	@PostMapping(value="/rent-car")
	public List<com.capstone.carrentalapi.dto.Car> findCarByVerifiedStatus(@RequestBody CarVerified carVerifiedStatus) {
		return carService.getCarByStatus(carVerifiedStatus);
	}

	@GetMapping("/getCarPendingVerification")
	public List<CarVerify> getCarPendingVerification() {
		return Arrays.asList(modelMapper.map(carService.getCarPendingVerification(), CarVerify[].class));
	}

	@PutMapping("/changeStatusCar")
	public ResponseEntity changeStatusCar(@RequestParam Integer idCar, @RequestParam Status status) {
		JsonObject jsonObject = new JsonObject();
		if (carService.changeCarStatus(idCar, status)) {
			jsonObject.addProperty("message", "Change status success");
			return ResponseEntity.status(HttpStatus.OK).body(jsonObject.toString());
		}
		jsonObject.addProperty("message", "Car not found");
		return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(jsonObject.toString());
	}

	@GetMapping("/getLocation")
	public List<CarLocation> getLocation(
			@RequestParam Integer idCar,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(value = "size", required = false, defaultValue = "20") Integer size) {
		Pageable pageable = PageRequest.of(page, size, Sort.by("date").descending());
		return Arrays.asList(modelMapper.map(carService.getLocation(idCar, pageable), CarLocation[].class));
	}

}
