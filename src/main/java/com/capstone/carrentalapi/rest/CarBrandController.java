package com.capstone.carrentalapi.rest;

import com.capstone.carrentalapi.dto.carbrand.CreateCarBrandRequest;
import com.capstone.carrentalapi.dto.carbrand.SimpleCarBrand;
import com.capstone.carrentalapi.dto.carcategory.CreateCarCategoryRequest;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import com.capstone.carrentalapi.service.CarBrandService;
import com.capstone.carrentalapi.service.CarCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/carBrands")
public class CarBrandController {

    private CarBrandService carBrandService;

    @Autowired
    public CarBrandController(CarBrandService carBrandService) {
        this.carBrandService = carBrandService;
    }

    @GetMapping
    public Collection<SimpleCarBrand> FindAll(){
        return carBrandService.FindAll();
    }

    @GetMapping("/FindById")
    public SimpleCarBrand FindById(Integer idBrand){
        return carBrandService.FindById(idBrand);
    }

    @PostMapping
    public SimpleCarBrand CreateCarBrand(CreateCarBrandRequest request){
        return carBrandService.CreateCarBrand(request);
    }

    @PutMapping
    public SimpleCarBrand UpdateCarBrand(SimpleCarBrand request){
        return carBrandService.UpdateCarBrand(request);
    }
}
