package com.capstone.carrentalapi.rest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capstone.carrentalapi.dto.transaction.FindTransaction;
import com.capstone.carrentalapi.service.TransactionService;

@RestController
@RequestMapping("/transaction")
public class TransactionController {
	
	private TransactionService transactionService;
	
	private ModelMapper modelMapper;
	
	@Autowired
	public TransactionController(TransactionService transactionService, ModelMapper modelMapper) {
		this.transactionService = transactionService;
		this.modelMapper = modelMapper;
	}
	
	@PostMapping(value="/update-transaction")
	public void updateByTransactionID(@RequestBody FindTransaction transaction) {
		transactionService.updateByTransactionID(transaction);
	}

}
