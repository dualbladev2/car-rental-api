package com.capstone.carrentalapi.rest;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capstone.carrentalapi.domain.model.CarStorage;
import com.capstone.carrentalapi.dto.carstorage.CarStorageDTO;
import com.capstone.carrentalapi.dto.carstorage.ModifyCarStorage;
import com.capstone.carrentalapi.dto.carstorage.UpdateCarStorage;
import com.capstone.carrentalapi.service.CarStorageService;

@RestController
@RequestMapping("/car-storage")
public class CarStorageController {
	
	private CarStorageService carStorageService;
	private final ModelMapper modelMapper;
	
	@Autowired
	public CarStorageController(CarStorageService carStorageService, ModelMapper modelMapper) {
		this.carStorageService = carStorageService;
		this.modelMapper = modelMapper;
	}
	
	@PostMapping
	public List<CarStorage> getCarStorage(@RequestBody CarStorageDTO carStorage) {
		return carStorageService.getStorage(carStorage);
	}
	
	@PostMapping(value="/delete-car-storage")
	public void deleteCarStorageByName(@RequestBody ModifyCarStorage storageName) {
		carStorageService.deleteCarStorageByName(storageName);
	}
	
	@PostMapping(value="/find-storage")
	public void findCarStorageByName(@RequestBody ModifyCarStorage storageName) {
		carStorageService.findCarStorageByName(storageName);
	}
	
	@PostMapping(value="/update-storage")
	public void updateCarStorageByName(@RequestBody UpdateCarStorage carStorage) {
		carStorageService.updateCarStorageByName(carStorage);
	}

}
