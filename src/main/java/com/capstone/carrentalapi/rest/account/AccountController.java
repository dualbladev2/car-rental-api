package com.capstone.carrentalapi.rest.account;

import com.capstone.carrentalapi.domain.enums.Status;
import com.capstone.carrentalapi.dto.Account;
import com.capstone.carrentalapi.dto.AccountRenter;
import com.capstone.carrentalapi.service.AccountService;
import com.google.firebase.auth.FirebaseToken;
import com.google.gson.JsonObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    private final ModelMapper modelMapper;
    private AccountService accountService;

    @Autowired
    public AccountController(ModelMapper modelMapper, AccountService accountService) {
        this.modelMapper = modelMapper;
        this.accountService = accountService;
    }

    @PostMapping(path = "/signUp")
    public Account signUp(@RequestAttribute("FirebaseToken") FirebaseToken firebaseToken) {
        return modelMapper.map(accountService.signUp(firebaseToken), Account.class);
    }

    @GetMapping
    public List<Account> getAll() {
        return Arrays.asList(modelMapper.map(accountService.findAll(), Account[].class));
    }

    @GetMapping(path = "/check")
    public Account getAccountByFirebaseId(@RequestAttribute("FirebaseToken") FirebaseToken firebaseToken) {
        return modelMapper.map(accountService.getAccountByFirebaseId(firebaseToken.getUid()), Account.class);
    }

    @GetMapping("/getRenterPendingVerification")
    public List<AccountRenter> getRenterPendingVerification() {
        return Arrays.asList(modelMapper.map(accountService.findAccountRenterByStatus(Status.PENDING), AccountRenter[].class));
    }

    @PutMapping("/changeStatusAccount")
    public ResponseEntity changeStatusAccount(@RequestParam Integer idAccount, @RequestParam Status status) {
        JsonObject jsonObject = new JsonObject();
        if (accountService.changeStatusAccount(idAccount, status)) {
            jsonObject.addProperty("message", "Change status success");
            return ResponseEntity.status(HttpStatus.OK).body(jsonObject.toString());
        }
        jsonObject.addProperty("message", "Account not found");
        return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(jsonObject.toString());
    }

}
