package com.capstone.carrentalapi.rest;

import java.text.ParseException;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capstone.carrentalapi.domain.model.TimeSlot;
import com.capstone.carrentalapi.dto.timeslot.PublishCar;
import com.capstone.carrentalapi.dto.timeslot.ReturnResult;
import com.capstone.carrentalapi.dto.timeslot.SearchCar;
import com.capstone.carrentalapi.dto.timeslot.TimeSlotRenting;
import com.capstone.carrentalapi.service.TimeSlotService;

@RestController
@RequestMapping("/timeslot")
public class TimeSlotController {
	
	private TimeSlotService timeSlotService;
	private ModelMapper modelMapper;
	
	@Autowired
	public TimeSlotController(TimeSlotService timeSlotService, ModelMapper modelMapper) {
		this.timeSlotService = timeSlotService;
		this.modelMapper = modelMapper;
	}
	
	@PostMapping(value="/rent-car-by-status")
	public List<TimeSlotRenting> rentCar(@RequestBody TimeSlotRenting rentingCar) throws ParseException {
		return timeSlotService.rentCarByStatus(rentingCar);
	}
	
	@PostMapping(value="/rent-car-by-timestamp")
	public List<TimeSlotRenting> rentCarByTimestamp(@RequestBody TimeSlotRenting rentingCar) throws ParseException {
		return timeSlotService.rentCarByTimestamp(rentingCar);
	}
	
	/*
	 * Publish Car - Add car to rent
	 */
	@PostMapping(value="/publish-car")
	public void publishCar(@RequestBody PublishCar publishCar) {
		timeSlotService.publishCar(publishCar);
	}
	
	/*
	 * Manage schedule
	 */
	
	/*
	 * Search and view car
	 */
	@PostMapping(value="/search-car")
	public List<ReturnResult> searchCarForDetail(@RequestBody SearchCar searchCar) {
		return timeSlotService.searchCar(searchCar);
	}

}
