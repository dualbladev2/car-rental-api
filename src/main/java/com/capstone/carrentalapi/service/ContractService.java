package com.capstone.carrentalapi.service;

import java.text.ParseException;
import java.util.List;

import com.capstone.carrentalapi.domain.model.Contract;
import com.capstone.carrentalapi.dto.contract.ContractResultById;
import com.capstone.carrentalapi.dto.contract.SearchContract;
import com.capstone.carrentalapi.dto.contract.SearchContractById;

public interface ContractService {
	
	List<Contract> searchContractByDate(SearchContract searchContract) throws ParseException;
	
	List<ContractResultById> findContractById(SearchContractById contractID);

}
