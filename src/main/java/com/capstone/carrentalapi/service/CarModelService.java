package com.capstone.carrentalapi.service;

import com.capstone.carrentalapi.dto.carmodel.CarModelWithBrandAndCategory;
import com.capstone.carrentalapi.dto.carmodel.SimpleCarModel;

import java.util.Collection;

public interface CarModelService {
    Collection<CarModelWithBrandAndCategory> FindAll();

    CarModelWithBrandAndCategory FindById(Integer id);

    CarModelWithBrandAndCategory FindByBrandAndCategory(Integer idBrand, Integer idCategory);

    CarModelWithBrandAndCategory UpdateCarModel(SimpleCarModel request);

    CarModelWithBrandAndCategory CreateCarModel(String name, String description, Integer idCategory, Integer idBrand);
}
