package com.capstone.carrentalapi.service;

import java.text.ParseException;
import java.util.List;

import com.capstone.carrentalapi.domain.model.TimeSlot;
import com.capstone.carrentalapi.dto.timeslot.PublishCar;
import com.capstone.carrentalapi.dto.timeslot.ReturnResult;
import com.capstone.carrentalapi.dto.timeslot.SearchCar;
import com.capstone.carrentalapi.dto.timeslot.TimeSlotRenting;

public interface TimeSlotService {
	
	List<TimeSlotRenting> rentCarByStatus(TimeSlotRenting rentingTime) throws ParseException;
	
	List<TimeSlotRenting> rentCarByTimestamp(TimeSlotRenting rentingTime) throws ParseException;
	
	void publishCar(PublishCar publishCar);

	List<ReturnResult> searchCar(SearchCar searchCar);
}
