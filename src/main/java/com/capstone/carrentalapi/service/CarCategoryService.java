package com.capstone.carrentalapi.service;

import com.capstone.carrentalapi.dto.carcategory.CreateCarCategoryRequest;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import com.capstone.carrentalapi.dto.carfeature.CreateCarFeatureRequest;
import com.capstone.carrentalapi.dto.carfeature.SimpleCarFeature;

import java.util.Collection;

public interface CarCategoryService {

    Collection<SimpleCarCategory> FindAll();

    SimpleCarCategory FindById(Integer idCategory);

    SimpleCarCategory CreateCarCategory(CreateCarCategoryRequest request);

    SimpleCarCategory UpdateCarCategory(SimpleCarCategory request);
}
