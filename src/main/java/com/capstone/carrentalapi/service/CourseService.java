package com.capstone.carrentalapi.service;

import com.capstone.carrentalapi.domain.model.Course;

import java.util.List;

public interface CourseService {

    List<Course> findAll();

    Course save(Course courseEntity);
    
    void addCourse (Course course);
    
    List<Course> findCourse (Course course);

}
