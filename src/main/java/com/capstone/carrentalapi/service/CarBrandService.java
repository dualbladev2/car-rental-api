package com.capstone.carrentalapi.service;

import com.capstone.carrentalapi.dto.carbrand.CreateCarBrandRequest;
import com.capstone.carrentalapi.dto.carbrand.SimpleCarBrand;
import com.capstone.carrentalapi.dto.carcategory.CreateCarCategoryRequest;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;

import java.util.Collection;

public interface CarBrandService {
    Collection<SimpleCarBrand> FindAll();

    SimpleCarBrand FindById(Integer idBrand);

    SimpleCarBrand CreateCarBrand(CreateCarBrandRequest request);

    SimpleCarBrand UpdateCarBrand(SimpleCarBrand request);
}
