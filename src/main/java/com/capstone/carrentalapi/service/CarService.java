package com.capstone.carrentalapi.service;

import com.capstone.carrentalapi.domain.enums.Status;
import java.util.List;
import java.util.Set;

import com.capstone.carrentalapi.domain.model.Car;
import com.capstone.carrentalapi.domain.model.Location;
import com.capstone.carrentalapi.dto.car.AddCar;
import com.capstone.carrentalapi.dto.car.CarVerified;
import com.capstone.carrentalapi.dto.car.LicensePlateCar;
import com.capstone.carrentalapi.dto.car.ModifyCar;
import com.capstone.carrentalapi.dto.car.UpdateCar;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface CarService {
	
	void addCar (AddCar car);
	
	void deleteCar (ModifyCar deleteCar);
	
	Car findCarByLicensePlate (LicensePlateCar carLicensePlate);
	
	void updateCarByLicensePlate (UpdateCar carLicensePlate);

    List<Car> getCarPendingVerification();

	boolean changeCarStatus(Integer idCar, Status status);

    List<Location> getLocation(Integer idCar, Pageable pageable);
	
	List<com.capstone.carrentalapi.dto.Car> getCarByStatus(CarVerified carVerifiedStatus);
}
