package com.capstone.carrentalapi.service.impl;

import com.capstone.carrentalapi.domain.model.CarCategory;
import com.capstone.carrentalapi.domain.model.CarFeature;
import com.capstone.carrentalapi.domain.repository.CarCategoryRepository;
import com.capstone.carrentalapi.dto.carcategory.CreateCarCategoryRequest;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import com.capstone.carrentalapi.dto.carfeature.SimpleCarFeature;
import com.capstone.carrentalapi.service.CarCategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class CarCategoryServiceImpl implements CarCategoryService {

    private CarCategoryRepository carCategoryRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public CarCategoryServiceImpl(CarCategoryRepository carCategoryRepository, ModelMapper modelMapper) {
        this.carCategoryRepository = carCategoryRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Collection<SimpleCarCategory> FindAll() {
        Collection<CarCategory> carCategories = carCategoryRepository.findAll();
        ArrayList<SimpleCarCategory> result = new ArrayList<>();

        for (CarCategory category : carCategories)
        {
            result.add(new SimpleCarCategory(category.getIdCategory(), category.getName(), category.getDescription()));
        }

        return result;
    }

    @Override
    public SimpleCarCategory FindById(Integer idCategory) {
        CarCategory carCategory = carCategoryRepository.findById(idCategory).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarCategoryServiceImpl.UpdateCarCategory(): Car Category not found."));
        return SimpleCarCategory.MapFromEntity(carCategory);
    }

    @Override
    public SimpleCarCategory CreateCarCategory(CreateCarCategoryRequest request) {
        CarCategory carCategory = new CarCategory();
        carCategory.setName(request.getName());
        carCategory.setDescription(request.getDescription());
        carCategory = carCategoryRepository.save(carCategory);

        return SimpleCarCategory.MapFromEntity(carCategory);
    }

    @Override
    public SimpleCarCategory UpdateCarCategory(SimpleCarCategory request) {
        CarCategory carCategory = carCategoryRepository.findById(request.getIdCategory()).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarCategoryServiceImpl.UpdateCarCategory(): Car Category not found."));

        carCategory.setName(request.getName());
        carCategory.setDescription(request.getDescription());
        carCategory = carCategoryRepository.save(carCategory);

        return SimpleCarCategory.MapFromEntity(carCategory);
    }
}
