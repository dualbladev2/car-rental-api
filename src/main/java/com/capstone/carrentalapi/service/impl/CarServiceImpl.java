package com.capstone.carrentalapi.service.impl;

import com.capstone.carrentalapi.domain.enums.Status;
import com.capstone.carrentalapi.domain.model.*;
import com.capstone.carrentalapi.domain.model.Car;
import com.capstone.carrentalapi.domain.model.CarModel;
import com.capstone.carrentalapi.domain.model.CarStorage;
import com.capstone.carrentalapi.domain.model.Owner;
import com.capstone.carrentalapi.domain.repository.CarRepository;
import com.capstone.carrentalapi.domain.repository.LocationRepository;
import com.capstone.carrentalapi.dto.car.AddCar;
import com.capstone.carrentalapi.dto.car.CarVerified;
import com.capstone.carrentalapi.dto.car.LicensePlateCar;
import com.capstone.carrentalapi.dto.car.ModifyCar;
import com.capstone.carrentalapi.dto.car.UpdateCar;
import com.capstone.carrentalapi.service.CarService;
import com.capstone.carrentalapi.util.DataConverter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CarServiceImpl implements CarService {

	private CarRepository carRepository;

	private ModelMapper modelMapper;

	private LocationRepository locationRepository;

	@Autowired
	public CarServiceImpl(CarRepository carRepository, ModelMapper modelMapper, LocationRepository locationRepository) {
		this.carRepository = carRepository;
		this.modelMapper = modelMapper;
		this.locationRepository = locationRepository;
	}

	public void addCar(AddCar car) {
		Timestamp now = new Timestamp(new Date().getTime());
		Car newCar = modelMapper.map(car, Car.class);
		newCar.setStatus(Status.PENDING);
		newCar.setCreatedDate(now);
		newCar.setLastUpdated(now);
		CarStorage carStorage = new CarStorage();
		carStorage.setIdStorage(car.getIdCarStorage());

		Owner carOwner = new Owner();
		carOwner.setIdAccount(car.getIdOwner());

		CarModel carModel = new CarModel();
		carModel.setIdModel(car.getIdCarModel());

		newCar.setCarStorage(carStorage);
		newCar.setCarModel(carModel);
		newCar.setOwner(carOwner);
		carRepository.save(newCar);
	}

	@Override
	public void deleteCar(ModifyCar deleteCar) {
		carRepository.deleteByIdCar(deleteCar.getIdCar());
		return;
	}

	@Override
	public Car findCarByLicensePlate(LicensePlateCar carLicensePlate) {
		return carRepository.findByLicensePlate(carLicensePlate.getLicensePlate());
	}

	@Override
	public void updateCarByLicensePlate(UpdateCar carLicensePlate) {
		Car updateCar = carRepository.findByLicensePlate(carLicensePlate.getLicensePlate());
		Timestamp now = new Timestamp(new Date().getTime());
		updateCar.setStatus(carLicensePlate.getStatus());
		updateCar.setDefaultPrice(carLicensePlate.getDefaultPrice());
		updateCar.setDistanceLimit(carLicensePlate.getDistanceLimit());
		updateCar.setSeat(carLicensePlate.getSeat());
		updateCar.setOverDistancePrice(carLicensePlate.getOverDistancePrice());
		updateCar.setPublicLatitude(carLicensePlate.getPublicLatitude());
		updateCar.setPublicLongitude(carLicensePlate.getPublicLongitude());
		updateCar.setDescription(carLicensePlate.getDescription());
		updateCar.setRentingTerm(carLicensePlate.getRentingTerm());
		updateCar.setFuelConsumption(carLicensePlate.getFuelConsumption());
		updateCar.setCompletedTrip(carLicensePlate.getCompletedTrip());
		updateCar.setLastUpdated(now);
		carRepository.save(updateCar);
	}

	@Override
	public List<Car> getCarPendingVerification() {
		return carRepository.findByStatus(Status.PENDING);
	}

	@Override
	public boolean changeCarStatus(Integer idCar, Status status) {
		if (carRepository.existsById(idCar)) {
			Car car = carRepository.getOne(idCar);
			car.setStatus(status);
			carRepository.save(car);
			return true;
		}
		return false;
	}

	@Override
	public List<Location> getLocation(Integer idCar, Pageable pageable) {
		if (carRepository.existsById(idCar)) {
			return locationRepository.findAllByCar_IdCar(idCar, pageable);
		}
		return new ArrayList<>();
	}

	@Override
	public List<com.capstone.carrentalapi.dto.Car> getCarByStatus(CarVerified carVerifiedStatus) {
		if (carVerifiedStatus.getStatus().equals(Status.VERIFIED.toString())) {
			List<Car> result = carRepository.findByStatus(carVerifiedStatus.getStatus());
			List<com.capstone.carrentalapi.dto.Car> finals = DataConverter.convertTemperatureDataToListTemperatureDataDTOs(result);
			return finals;
		} else {
			return null;
		}
	}


}
