package com.capstone.carrentalapi.service.impl;

import com.capstone.carrentalapi.domain.model.CarBrand;
import com.capstone.carrentalapi.domain.model.CarCategory;
import com.capstone.carrentalapi.domain.repository.CarBrandRepository;
import com.capstone.carrentalapi.dto.carbrand.CreateCarBrandRequest;
import com.capstone.carrentalapi.dto.carbrand.SimpleCarBrand;
import com.capstone.carrentalapi.dto.carcategory.CreateCarCategoryRequest;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import com.capstone.carrentalapi.service.CarBrandService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class CarBrandServiceImpl implements CarBrandService {

    private CarBrandRepository carBrandRepository;

    private final ModelMapper modelMapper;

    public CarBrandServiceImpl(CarBrandRepository carBrandRepository, ModelMapper modelMapper) {
        this.carBrandRepository = carBrandRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Collection<SimpleCarBrand> FindAll() {
        Collection<CarBrand> carBrands = carBrandRepository.findAll();
        ArrayList<SimpleCarBrand> result = new ArrayList<>();

        for (CarBrand brand : carBrands)
        {
            result.add(new SimpleCarBrand(brand.getIdBrand(), brand.getName(), brand.getDescription()));
        }

        return result;
    }

    @Override
    public SimpleCarBrand FindById(Integer idBrand) {
        CarBrand carBrand = carBrandRepository.findById(idBrand).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarBrandServiceImpl.FindById(): Car Brand not found."));
        return SimpleCarBrand.MapFromEntity(carBrand);
    }

    @Override
    public SimpleCarBrand CreateCarBrand(CreateCarBrandRequest request) {
        CarBrand carBrand = new CarBrand();
        carBrand.setName(request.getName());
        carBrand.setDescription(request.getDescription());
        carBrand = carBrandRepository.save(carBrand);

        return SimpleCarBrand.MapFromEntity(carBrand);
    }

    @Override
    public SimpleCarBrand UpdateCarBrand(SimpleCarBrand request) {
        CarBrand carBrand = carBrandRepository.findById(request.getIdBrand()).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarBrandServiceImpl.UpdateCarBrand(): Car Brand not found."));

        carBrand.setName(request.getName());
        carBrand.setDescription(request.getDescription());
        carBrand = carBrandRepository.save(carBrand);

        return SimpleCarBrand.MapFromEntity(carBrand);
    }
}
