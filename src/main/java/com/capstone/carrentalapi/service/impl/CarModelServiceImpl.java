package com.capstone.carrentalapi.service.impl;

import com.capstone.carrentalapi.domain.model.Car;
import com.capstone.carrentalapi.domain.model.CarBrand;
import com.capstone.carrentalapi.domain.model.CarCategory;
import com.capstone.carrentalapi.domain.model.CarModel;
import com.capstone.carrentalapi.domain.repository.CarBrandRepository;
import com.capstone.carrentalapi.domain.repository.CarCategoryRepository;
import com.capstone.carrentalapi.domain.repository.CarModelRepository;
import com.capstone.carrentalapi.dto.carbrand.SimpleCarBrand;
import com.capstone.carrentalapi.dto.carcategory.SimpleCarCategory;
import com.capstone.carrentalapi.dto.carmodel.CarModelWithBrandAndCategory;
import com.capstone.carrentalapi.dto.carmodel.SimpleCarModel;
import com.capstone.carrentalapi.service.CarModelService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class CarModelServiceImpl implements CarModelService {

    private CarModelRepository carModelRepository;
    private CarBrandRepository carBrandRepository;
    private CarCategoryRepository carCategoryRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public CarModelServiceImpl(CarModelRepository carModelRepository, CarBrandRepository carBrandRepository, CarCategoryRepository carCategoryRepository, ModelMapper modelMapper) {
        this.carModelRepository = carModelRepository;
        this.carBrandRepository = carBrandRepository;
        this.carCategoryRepository = carCategoryRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Collection<CarModelWithBrandAndCategory> FindAll() {
        Collection<CarModel> carModels = carModelRepository.findAll();
        ArrayList<CarModelWithBrandAndCategory> result = new ArrayList<>();

        for (CarModel carModel: carModels)
        {
            result.add(CarModelWithBrandAndCategory.MapFromEntity(carModel));
        }

        return result;
    }

    @Override
    public CarModelWithBrandAndCategory FindById(Integer id) {
        CarModel carModel = carModelRepository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarModelServiceImpl.FindById(): Car Model Not Found"));
        return CarModelWithBrandAndCategory.MapFromEntity(carModel);
    }

    @Override
    public CarModelWithBrandAndCategory FindByBrandAndCategory(Integer idBrand, Integer idCategory) {
        List<CarModel> carModels = carModelRepository.findByBrandAndCategory(idBrand, idCategory);

        if (carModels.size() <= 0){
            return null;
        }

        return CarModelWithBrandAndCategory.MapFromEntity(carModels.get(0));
    }

    @Override
    public CarModelWithBrandAndCategory UpdateCarModel(SimpleCarModel request) {
        CarModel carModel = carModelRepository.findById(request.getIdModel()).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarModelServiceImpl.UpdateCarModel(): Car Model Not Found"));
        //get brand and category
        CarBrand brand = carBrandRepository.findById(request.getIdBrand()).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarModelServiceImpl.UpdateCarModel(): Car Brand Not Found"));
        CarCategory category = carCategoryRepository.findById(request.getIdCategory()).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarModelServiceImpl.UpdateCarModel(): Car Category Not Found"));

        carModel.setName(request.getName());
        carModel.setDescription(request.getDescription());
        carModel.setCarCategory(category);
        carModel.setCarBrand(brand);
        carModel = carModelRepository.save(carModel);

        //Map entity to model
        return CarModelWithBrandAndCategory.MapFromEntity(carModel);
    }

    @Override
    public CarModelWithBrandAndCategory CreateCarModel(String name, String description, Integer idCategory, Integer idBrand) {
        //get brand and category
        CarBrand brand = carBrandRepository.findById(idBrand).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarModelServiceImpl.CreateCarModel(): Car Brand Not Found"));
        CarCategory category = carCategoryRepository.findById(idCategory).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarModelServiceImpl.CreateCarModel(): Car Category Not Found"));

        //Create new car model
        CarModel carModel = new CarModel();
        carModel.setName(name);
        carModel.setDescription(description);
        carModel.setCarBrand(brand);
        carModel.setCarCategory(category);
        carModel = carModelRepository.save(carModel);

        //Map entity to model
        return CarModelWithBrandAndCategory.MapFromEntity(carModel);
    }
}
