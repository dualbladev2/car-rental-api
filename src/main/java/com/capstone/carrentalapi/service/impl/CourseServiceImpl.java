package com.capstone.carrentalapi.service.impl;

import com.capstone.carrentalapi.domain.model.Course;
import com.capstone.carrentalapi.domain.repository.CourseRepository;
import com.capstone.carrentalapi.service.CourseService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {

    private CourseRepository courseRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository, ModelMapper modelMapper) {
        this.courseRepository = courseRepository;
        this.modelMapper = modelMapper;
    }

	public List<Course> findAll() {
		return courseRepository.findAll();
	}

	public Course save(Course course) {
		return null;
	}

	public void addCourse(Course course) {
		Course newCourse = new Course();
		newCourse.setId(course.getId());
		newCourse.setName(course.getName());
		courseRepository.save(newCourse);
		
	}

	public List<Course> findCourse(Course course) {
		return courseRepository.findByName(course.getName());
	}

}
