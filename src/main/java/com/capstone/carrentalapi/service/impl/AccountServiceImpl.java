package com.capstone.carrentalapi.service.impl;

import com.capstone.carrentalapi.domain.enums.Status;
import com.capstone.carrentalapi.domain.model.Account;
import com.capstone.carrentalapi.domain.model.Owner;
import com.capstone.carrentalapi.domain.model.PersonalInformation;
import com.capstone.carrentalapi.domain.model.Renter;
import com.capstone.carrentalapi.domain.repository.AccountRepository;
import com.capstone.carrentalapi.domain.repository.OwnerRepository;
import com.capstone.carrentalapi.domain.repository.PersonalInformationRepository;
import com.capstone.carrentalapi.domain.repository.RenterRepository;
import com.capstone.carrentalapi.service.AccountService;
import com.google.firebase.auth.FirebaseToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    AccountRepository accountRepository;
    RenterRepository renterRepository;
    OwnerRepository ownerRepository;
    PersonalInformationRepository personalInformationRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, RenterRepository renterRepository, OwnerRepository ownerRepository, PersonalInformationRepository personalInformationRepository) {
        this.accountRepository = accountRepository;
        this.renterRepository = renterRepository;
        this.ownerRepository = ownerRepository;
        this.personalInformationRepository = personalInformationRepository;
    }

    @Override
    public Account signUp(FirebaseToken firebaseToken) {
        Timestamp now = new Timestamp(new Date().getTime());
        Account account = new Account();
        account.setCreatedDate(now);
        account.setLastUpdate(now);
        account.setFirebaseId(firebaseToken.getUid());
        try {
            account = accountRepository.save(account);
        } catch (Exception e) {
            account = accountRepository.getAccountByFirebaseId(firebaseToken.getUid());
        }
        PersonalInformation personalInformation = new PersonalInformation();
        personalInformation.setIdAccount(account.getIdAccount());
        personalInformation.setEmail(firebaseToken.getEmail());
        personalInformation.setFullName(firebaseToken.getName());
        personalInformation.setCreatedDate(now);
        personalInformation.setLastUpdate(now);
        personalInformationRepository.save(personalInformation);
        try {
            personalInformation = personalInformationRepository.save(personalInformation);
        } catch (Exception e) {
            personalInformation = personalInformationRepository.getOne(account.getIdAccount());
        }
        Renter renter = new Renter();
        renter.setIdAccount(account.getIdAccount());
        renterRepository.save(renter);
        try {
            renter = renterRepository.save(renter);
        } catch (Exception e) {
            renter = renterRepository.getOne(account.getIdAccount());
        }

        Owner owner = new Owner();
        owner.setIdAccount(account.getIdAccount());
        ownerRepository.save(owner);
        try {
            owner = ownerRepository.save(owner);
        } catch (Exception e) {
            owner = ownerRepository.getOne(account.getIdAccount());
        }
        return account;
    }

    @Override
    public Account getAccountByFirebaseId(String firebaseId) {
        return accountRepository.getAccountByFirebaseId(firebaseId);
    }

    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }


    @Override
    public List<Account> findAccountRenterByStatus(Status status) {
        return accountRepository.findAccountRenterByStatus(status);
    }

    @Override
    public boolean changeStatusAccount(Integer idAccount, Status status) {
        if (accountRepository.existsById(idAccount)) {
            Account account = accountRepository.getOne(idAccount);
            account.setStatus(status);
            accountRepository.save(account);
            return true;
        }
        return false;
    }
}
