package com.capstone.carrentalapi.service.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capstone.carrentalapi.domain.enums.CarRentStatus;
import com.capstone.carrentalapi.domain.enums.Status;
import com.capstone.carrentalapi.domain.model.Car;
import com.capstone.carrentalapi.domain.model.TimeSlot;
import com.capstone.carrentalapi.domain.repository.CarRepository;
import com.capstone.carrentalapi.domain.repository.TimeSlotRepository;
import com.capstone.carrentalapi.dto.timeslot.PublishCar;
import com.capstone.carrentalapi.dto.timeslot.ReturnResult;
import com.capstone.carrentalapi.dto.timeslot.SearchCar;
import com.capstone.carrentalapi.dto.timeslot.TimeSlotRenting;
import com.capstone.carrentalapi.service.TimeSlotService;
import com.capstone.carrentalapi.util.DataConverter;

@Service
@Transactional
public class TimeSlotServiceImpl implements TimeSlotService {
	
	private CarRepository carRepository;
	
	private TimeSlotRepository timeSlotRepository;
	
	private ModelMapper modelMapper;
	
	private CarRentStatus carRentStatus;
	
	@Autowired
	public TimeSlotServiceImpl(TimeSlotRepository timeSlotRepository, ModelMapper modelMapper, CarRepository carRepository) {
		this.timeSlotRepository = timeSlotRepository;
		this.modelMapper = modelMapper;
		this.carRepository = carRepository;
	}

	@Override
	public List<TimeSlotRenting> rentCarByStatus(TimeSlotRenting rentingTime) throws ParseException  {
		if (rentingTime.getStatus().equals(CarRentStatus.AVAILABLE.toString())) {
			List<TimeSlot> result = timeSlotRepository.findByStatus(rentingTime.getStatus());
			List<TimeSlotRenting> test = DataConverter.convertTimeSlotDataToListTimeSlotDataDTOs(result);
			return test;
		} else {
			return null;
		}
	}

	@Override
	public void publishCar(PublishCar publishCar) {
		TimeSlot publishNewCar = modelMapper.map(publishCar, TimeSlot.class);
		publishAvailableCar(publishCar, publishNewCar, carRentStatus);
		
	}

	private void publishAvailableCar(PublishCar publishCar, TimeSlot publishNewCar, CarRentStatus carRentStatus) {
		Car car = new Car();
		car.setIdCar(publishCar.getIdCar());
		publishNewCar.setCar(car);
		publishNewCar.setPrice(publishCar.getPrice());
		publishNewCar.setStatus(carRentStatus.AVAILABLE.toString());
		
		/*
		 * Get input for start-date and end-date
		 * 1 slot in 24 hours only 
		 */
		// 1 . Get input of start-date
		// 2 . Convert into LocalDateTime
		// 3 . Convert into Timestamp
		
		
		java.util.Date startDate = new java.util.Date();
		startDate = publishCar.getStartDate();
		
		LocalDateTime ldtStartDate = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		ldtStartDate.with(LocalTime.MIN);
		Timestamp start = Timestamp.valueOf(ldtStartDate);
		
		java.util.Date endDate = new java.util.Date();
		endDate = publishCar.getEndDate();
		LocalDateTime ldtEndDate = LocalDateTime.ofInstant(endDate.toInstant(), ZoneId.systemDefault());
		
		// Compare two variable - compare 2 end date value
		if (ldtEndDate.with(LocalTime.MAX).isAfter(ldtStartDate.with(LocalTime.MAX))) {
			ldtEndDate.with(ldtStartDate.with(LocalTime.MAX));
			Timestamp end = Timestamp.valueOf(ldtStartDate.with(LocalTime.MAX));
			publishNewCar.setStartDate(start);
			publishNewCar.setEndDate(end);
			timeSlotRepository.save(publishNewCar);
		} else {
			ldtEndDate.with(LocalTime.MAX);
			Timestamp end = Timestamp.valueOf(ldtEndDate);
			publishNewCar.setStartDate(start);
			publishNewCar.setEndDate(end);
			timeSlotRepository.save(publishNewCar);
		}
	}

	@Override
	public List<TimeSlotRenting> rentCarByTimestamp(TimeSlotRenting rentingTime) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date startDate = (java.util.Date) dateFormat.parse(rentingTime.getStartDate());
		Timestamp start = new Timestamp(startDate.getTime());
		
		java.util.Date endDate = (java.util.Date) dateFormat.parse(rentingTime.getEndDate());
		Timestamp end = new Timestamp(endDate.getTime());
		
		List<TimeSlot> result = timeSlotRepository.findByStartDateBetween(start, end);
		List<TimeSlotRenting> test = DataConverter.convertTimeSlotDataToListTimeSlotDataDTOs(result);
		return test;
	}

	@Override
	public List<ReturnResult> searchCar(SearchCar searchCar) {
		List<TimeSlot> result = timeSlotRepository.findByStatusAndCarIdCar(searchCar.getStatus(), searchCar.getCarID());
		List<ReturnResult> test = DataConverter.convertDataReturnToListDataReturnDTOs(result);
		return test;
	}	

}
