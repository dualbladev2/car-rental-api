package com.capstone.carrentalapi.service.impl;

import com.capstone.carrentalapi.domain.model.Car;
import com.capstone.carrentalapi.domain.model.CarFeature;
import com.capstone.carrentalapi.domain.model.CarHaveFeature;
import com.capstone.carrentalapi.domain.repository.CarFeatureRepository;
import com.capstone.carrentalapi.domain.repository.CarHaveFeatureRepository;
import com.capstone.carrentalapi.domain.repository.CarRepository;
import com.capstone.carrentalapi.dto.car.CarWithFeature;
import com.capstone.carrentalapi.dto.carfeature.CreateCarFeatureRequest;
import com.capstone.carrentalapi.dto.carfeature.SimpleCarFeature;
import com.capstone.carrentalapi.service.CarFeatureService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class CarFeatureServiceImpl implements CarFeatureService {

    private CarFeatureRepository carFeatureRepository;
    private CarRepository carRepository;
    private CarHaveFeatureRepository carHaveFeatureRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public CarFeatureServiceImpl(CarFeatureRepository carFeatureRepository, CarRepository carRepository, CarHaveFeatureRepository carHaveFeatureRepository, ModelMapper modelMapper) {
        this.carFeatureRepository = carFeatureRepository;
        this.carRepository = carRepository;
        this.carHaveFeatureRepository = carHaveFeatureRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Collection<SimpleCarFeature> FindAll() {
        Collection<CarFeature> carFeatures = carFeatureRepository.findAll();
        ArrayList<SimpleCarFeature> result = new ArrayList<>();

        for (CarFeature feature : carFeatures)
        {
            result.add(new SimpleCarFeature(feature.getIdFeature(), feature.getName(), feature.getDescription()));
        }

        return result;
    }

    @Override
    public SimpleCarFeature FindById(Integer idFeature) {
        CarFeature carFeature = carFeatureRepository.findById(idFeature).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarFeatureServiceImpl.FindById(): Car Feature not found."));
        return new SimpleCarFeature(carFeature.getIdFeature(), carFeature.getName(), carFeature.getDescription());
    }

    @Override
    public SimpleCarFeature CreateCarFeature(CreateCarFeatureRequest request) {
        CarFeature carFeature = new CarFeature();
        carFeature.setName(request.getName());
        carFeature.setDescription(request.getDescription());
        carFeature = carFeatureRepository.save(carFeature);

        return SimpleCarFeature.MapFromEntity(carFeature);
    }

    @Override
    public SimpleCarFeature UpdateCarFeature(SimpleCarFeature request) {
        CarFeature carFeature = carFeatureRepository.findById(request.getIdFeature()).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarFeatureServiceImpl.UpdateCarFeature(): Car Feature not found."));

        carFeature.setName(request.getName());
        carFeature.setDescription(request.getDescription());
        carFeature = carFeatureRepository.save(carFeature);

        return SimpleCarFeature.MapFromEntity(carFeature);
    }

    @Override
    public CarWithFeature UpdateFeatureOfCar(Integer idCar, Collection<Integer> idFeatures) {
        //Get Car entity
        Car car = carRepository.findById(idCar).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "CarFeatureServiceImpl.UpdateFeatureOfCar(): Car not found."));

        ArrayList<CarFeature> pendingCarFeatures = (ArrayList<CarFeature>) carFeatureRepository.findAllById(idFeatures);
        ArrayList<CarHaveFeature> pendingCarHaveFeatures = new ArrayList<>();

        CarWithFeature result = new CarWithFeature();
        result.setIdCar(idCar);

        for (CarFeature pendingFeature: pendingCarFeatures)
        {
            CarHaveFeature carHaveFeature = new CarHaveFeature();
            carHaveFeature.setCar(car);
            carHaveFeature.setCarFeature(pendingFeature);
            pendingCarHaveFeatures.add(carHaveFeature);

            //Add feature to result list
            result.getFeatures().add(new SimpleCarFeature(pendingFeature.getIdFeature(), pendingFeature.getName(), pendingFeature.getDescription()));

        }

        car.setCarHaveFeatures(pendingCarHaveFeatures);
        carRepository.save(car);

        return result;

//        if (!carHaveFeatures.isEmpty())
//        {
//            for (CarHaveFeature feature : car.getCarHaveFeatures())
//            {
//                //If a feature doesn't exist in new list of feature, remove it from car
//                if (!pendingCarFeatures.contains(feature.getCarFeature())){
//                    car.getCarHaveFeatures().remove(feature);
//                }
//                //If it does exist, remove from list of pending features so it won't be checked again.
//                else {
//                    pendingCarFeatures.remove(feature.getCarFeature());
//                }
//            }
//        }
//
//        car.setCarHaveFeatures();


    }
}
