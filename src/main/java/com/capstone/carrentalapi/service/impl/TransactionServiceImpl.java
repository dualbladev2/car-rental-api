package com.capstone.carrentalapi.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capstone.carrentalapi.domain.model.Transaction;
import com.capstone.carrentalapi.domain.repository.TransactionRepository;
import com.capstone.carrentalapi.dto.transaction.FindTransaction;
import com.capstone.carrentalapi.service.TransactionService;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
	
	private TransactionRepository transactionRepository;
	
	private ModelMapper modelMapper;
	
	@Autowired
	public TransactionServiceImpl(TransactionRepository transactionRepository, ModelMapper modelMapper) {
		this.transactionRepository = transactionRepository;
		this.modelMapper = modelMapper;
	}

	@Override
	public void updateByTransactionID(FindTransaction transaction) {
		Transaction transactionUpdate = transactionRepository.findByIdTransaction(transaction.getTransactionID());
		transactionUpdate.setDescription(transaction.getTransactionDescription());
		transactionUpdate.getContract().setAdvanceMoney(transaction.getAdvanceMoney());
		transactionRepository.save(transactionUpdate);
	}

}
