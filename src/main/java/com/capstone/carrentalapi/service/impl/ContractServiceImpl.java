package com.capstone.carrentalapi.service.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capstone.carrentalapi.domain.model.Contract;
import com.capstone.carrentalapi.domain.repository.ContractRepository;
import com.capstone.carrentalapi.dto.contract.ContractResultById;
import com.capstone.carrentalapi.dto.contract.SearchContract;
import com.capstone.carrentalapi.dto.contract.SearchContractById;
import com.capstone.carrentalapi.service.ContractService;
import com.capstone.carrentalapi.util.DataConverter;

@Service
@Transactional
public class ContractServiceImpl implements ContractService {
	
	private ContractRepository contractRepository;
	
	private ModelMapper modelMapper;
	
	@Autowired
	public ContractServiceImpl(ContractRepository contractRepository, ModelMapper modelMapper) {
		this.contractRepository = contractRepository;
		this.modelMapper = modelMapper;
	}

	@Override
	public List<Contract> searchContractByDate(SearchContract searchContract) throws ParseException {
		String dateInput = searchContract.getDateInput();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		Date date = formatter.parse(dateInput);
		Timestamp timeStampDateInput = new Timestamp(date.getTime());
		List<Contract> contracts = contractRepository.findByCreateDate(timeStampDateInput);
		return contracts;
	}

	@Override
	public List<ContractResultById> findContractById(SearchContractById contractID) {
		List<Contract> result =  contractRepository.findByIdContract(contractID.getContractID());
		List<ContractResultById> finals = DataConverter.convertContractDataToListContractDataDTOs(result);
		return finals;
	}

}
