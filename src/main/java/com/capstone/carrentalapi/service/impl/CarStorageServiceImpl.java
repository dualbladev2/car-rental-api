package com.capstone.carrentalapi.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capstone.carrentalapi.domain.model.CarStorage;
import com.capstone.carrentalapi.domain.repository.CarStorageRepository;
import com.capstone.carrentalapi.dto.carstorage.CarStorageDTO;
import com.capstone.carrentalapi.dto.carstorage.ModifyCarStorage;
import com.capstone.carrentalapi.dto.carstorage.UpdateCarStorage;
import com.capstone.carrentalapi.service.CarStorageService;

@Service
@Transactional
public class CarStorageServiceImpl implements CarStorageService {
	
	private CarStorageRepository carStorageRepository;
	private ModelMapper modelMapper;
	
	@Autowired
	public CarStorageServiceImpl(CarStorageRepository carStorageRepository, ModelMapper modelMapper) {
		this.carStorageRepository = carStorageRepository;
		this.modelMapper = modelMapper;
	}
	

	public List<CarStorage> getStorage(CarStorageDTO carStorage) {
		return carStorageRepository.findByOwnerIdAccount(carStorage.getIdOwner());
	}


	@Override
	public void deleteCarStorageByName(ModifyCarStorage storageName) {
		carStorageRepository.deleteByName(storageName.getStorageName());
	}


	@Override
	public void findCarStorageByName(ModifyCarStorage storageName) {
		carStorageRepository.findByName(storageName.getStorageName());
	}


	@Override
	public void updateCarStorageByName(UpdateCarStorage storageName) {
		CarStorage carStorage = carStorageRepository.findByName(storageName.getStorageName());
		carStorage.setCarNumber(storageName.getCarNumber());
		carStorage.setLatitude(storageName.getLatitude());
		carStorage.setLongitude(storageName.getLongitude());
		Timestamp now = new Timestamp(new Date().getTime());
		carStorageRepository.save(carStorage);
		
	}
	
	

}
