package com.capstone.carrentalapi.service;

import com.capstone.carrentalapi.dto.car.CarWithFeature;
import com.capstone.carrentalapi.dto.carfeature.CreateCarFeatureRequest;
import com.capstone.carrentalapi.dto.carfeature.SimpleCarFeature;

import java.util.Collection;

public interface CarFeatureService {
    Collection<SimpleCarFeature> FindAll();

    SimpleCarFeature FindById(Integer idFeature);

    SimpleCarFeature CreateCarFeature(CreateCarFeatureRequest request);

    SimpleCarFeature UpdateCarFeature(SimpleCarFeature request);

    CarWithFeature UpdateFeatureOfCar(Integer idCar, Collection<Integer> idFeatures);
}
