package com.capstone.carrentalapi.service;


import com.capstone.carrentalapi.domain.enums.Status;
import com.capstone.carrentalapi.domain.model.Account;
import com.google.firebase.auth.FirebaseToken;

import java.util.List;

public interface AccountService {

    Account signUp(FirebaseToken firebaseToken);

    Account getAccountByFirebaseId(String firebaseId);

    List<Account> findAll();

    List<Account> findAccountRenterByStatus(Status status);

    boolean changeStatusAccount(Integer idAccount, Status status);
}
