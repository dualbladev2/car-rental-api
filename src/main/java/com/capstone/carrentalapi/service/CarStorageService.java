package com.capstone.carrentalapi.service;

import java.util.List;

import com.capstone.carrentalapi.domain.model.CarStorage;
import com.capstone.carrentalapi.dto.carstorage.CarStorageDTO;
import com.capstone.carrentalapi.dto.carstorage.ModifyCarStorage;
import com.capstone.carrentalapi.dto.carstorage.UpdateCarStorage;

public interface CarStorageService {
	
	List<CarStorage> getStorage(CarStorageDTO carStorage);

	void deleteCarStorageByName(ModifyCarStorage storageName);
	
	void findCarStorageByName(ModifyCarStorage storageName);
	
	void updateCarStorageByName(UpdateCarStorage storageName);
}
