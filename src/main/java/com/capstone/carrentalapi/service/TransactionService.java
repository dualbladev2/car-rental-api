package com.capstone.carrentalapi.service;

import com.capstone.carrentalapi.dto.transaction.FindTransaction;

public interface TransactionService {
	
	void updateByTransactionID(FindTransaction transactionID);

}
