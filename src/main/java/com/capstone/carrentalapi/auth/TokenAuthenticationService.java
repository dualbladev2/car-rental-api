package com.capstone.carrentalapi.auth;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

public class TokenAuthenticationService {
    static final String TOKEN_PREFIX = "Bearer ";

    static final String HEADER_STRING = "Authorization";

    public static Authentication getAuthentication(HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authOk = new UsernamePasswordAuthenticationToken(null, null, Collections.emptyList());
        UsernamePasswordAuthenticationToken authNotOk = new UsernamePasswordAuthenticationToken(null, null);
//        UsernamePasswordAuthenticationToken authNotOk = authOk;
        boolean isGetMethod = request.getMethod().equals(RequestMethod.GET.toString());
        try {
            String token = request.getHeader(HEADER_STRING);
            if (token != null) {
                if (!token.isEmpty()) {
                    token = token.replace(TOKEN_PREFIX, "");
                    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

                    boolean checkRevoke = false;
                    checkRevoke = !request.getMethod().equals(HttpMethod.GET);

                    FirebaseToken firebaseToken = firebaseAuth.verifyIdToken(token, checkRevoke);

                    request.setAttribute("FirebaseToken", firebaseToken);
                    System.out.println("ok: " + firebaseToken.getUid());
                    return authOk;
                }
            }
            System.out.println("no auth header" + "-" + request.getMethod() + "-" + RequestMethod.GET + "=" + isGetMethod);
            if (isGetMethod) {
                return authOk;
            } else {
                return authNotOk;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception");
            if (isGetMethod) {
                return authOk;
            } else {
                return authNotOk;
            }
        }
    }
}
